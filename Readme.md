# Exemple d'application pour kubernetes

## Récupération du projet 

Pour récupérer le projet, il suffit de le cloner avec git :

```sh
git clone https://gitlab.com/docker-exemple/chat_k8s.git
```
## Lancement de l'application

### Avec docker compose :

On peut utiliser l'application sans cluster kubernetes en utilisant 
`docker compose` (cf. fichier [compose.yaml](compose.yaml)) :  

```sh
docker compose up -d 
```

Lors du premier démarrage, la base de données est automatiquement initialisée 
(création de la base et des tables) en utilisant la sauvegarde [chat.mysql](chat.mysql).

L'application est alors accessible en utilisant l'url 
[http://localhost:9000/login.php](http://localhost:9000/login.php) (choisir le chat d'id 1) 
ou l'url 
[http://localhost:9000/chat.php?pseudo=Joe&chat=1](http://localhost:9000/chat.php?pseudo=Joe&chat=1).

### Avec kubernetes :

Pour utiliser l'application dans un cluster kubernetes, on peut utiliser 
les fichiers de configuration présents dans le dossier [k8s](k8s) et déployer 
toute l'application en une seule fois en utilisant la commande 
[`kubectl apply`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) : 

```sh
kubectl apply -f k8s/
```

**Remarque :** La dernière version du projet suppose l'utilisation d'un [`LoadBalancer`](#attribution-dune-adresse-ip-externe-au-service) 
et d'un [`Ingress`](#utilisation-dun-ingress),
qui doivent être adapté à votre contexte de déploiement.


Lors du premier démarrage, la base de données est automatiquement initialisée 
(création de la base et des tables) en utilisant la sauvegarde [chat.mysql](chat.mysql). 

Si vous utilisez minikube, vous pouvez ouvrir l'application 
dans un navigateur avec la commande `minikube service chat` 
(via la création d'un tunnel ssh avec le cluster minikube).

## Détails de la version Kubernetes

La version kubernetes est structurée comme la version docker compose. 

Pour chaque `service` docker compose, on a un [`deployment`](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) 
et un [`service`](https://kubernetes.io/docs/concepts/services-networking/service/) k8s : 
- base mysql :
    - déploiement : [k8s/mysql-dep.yaml](k8s/mysql-dep.yaml)
    - service (de type [`ClusterIp`](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types), interne au cluster) : [k8s/mysql-svc.yaml](k8s/mysql-svc.yaml))
- chat :
    - déploiement :  [k8s/chat-dep.yaml](k8s/chat-dep.yaml)
    - service (de type [`LoadBalancer`](https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer)) :  [k8s/chat-svc.yaml](k8s/chat-svc.yaml) -- nécessite d'avoir un LoadBalancer disponible --  
    ou de type [`NodePort`](https://kubernetes.io/docs/concepts/services-networking/service/#type-nodeport)) :  voir exemple ci-dessous)
```yml
apiVersion: v1
kind: Service
metadata:
  name: chat-svc
  labels:
    app: chat
    tiers: backend
spec:
  type: NodePort
  selector:
    app: chat
    lang: php
  ports:
  - port: 80
    targetPort: 80
    nodePort: 32123
```

Un [secret kubernetes](https://kubernetes.io/docs/concepts/configuration/secret/) (de type `Opaque`), [k8s/mysql-secret.yaml](k8s/mysql-secret.yaml) 
permet de communiquer, de façon sécurisée, le mot de passe root de la base de données aux pods des deux 
déploiements. 

Dans le cas du chat php, ce secret est monté dans un fichier (`/run/secrets/mysql/password`) :

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: chat
    tiers: backend
  name: chat
spec:
  selector:
    matchLabels:
      app: chat
      lang: php
  template:
    metadata:
      labels:
        app: chat
        lang: php
    spec:
      containers:
      - name: chat
        image: registry.gitlab.com/docker-exemple/chat_k8s/chat:v2.1.0
        ...
        ...
        volumeMounts:
          - name: mysql-secret
            mountPath: "/run/secrets/mysql"
      volumes:
        - name: mysql-secret
          secret:
            secretName: mysql-secret
            items:
              - key: password
                path: password
```

Alors qu'il est fournit comme une variable d'environnement (`MYSQL_ROOT_PASSWORD`) au conteneur mysql : 

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql
  labels:
    app: chat
    tiers: bd
spec:
  selector:
    matchLabels:
      app: chat
      tiers: bd
  template:
    metadata:
      labels:
        app: chat
        tiers: bd
    spec:
      containers:
      - name: mysql
        image: "docker.io/library/mysql:8.4"
        env:
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              key: password
              name: mysql-secret
        ...
        ...
```

L'initialisation de la base de données, lors du premier démarrage, se fait au moyen d'un objet de type 
[`ConfigMap`](https://kubernetes.io/docs/concepts/configuration/configmap/),
[k8s/mysql-conf.yaml](k8s/mysql-conf.yaml), monté dans `/docker-entrypoint-initdb.d/init-db.sql` 
(ici aussi c'est très similaire à ce qui est fait dans la version `docker compose`).

Enfin, un [volume persistent](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistent-volumes), 
est monté dans le conteneur mysql afin d'assure la permanence des données de la base.

Ce volume persistent est obtenu au moyen d'un objet de type 
[`PersistenceVolumeClaim`](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims), 
[k8s/mysql-pvc.yaml](k8s/mysql-pvc.yaml), puis est monté dans le répertoire, `/val/lib/mysql` du conteneur mysql : 

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql
  labels:
    app: chat
    tiers: bd
spec:
  selector:
    matchLabels:
      app: chat
      tiers: bd
  template:
    metadata:
      labels:
        app: chat
        tiers: bd
    spec:
      containers:
      - name: mysql
        image: "docker.io/library/mysql:8.4"
        ...
        ...
        volumeMounts:
          - name: mysql-data
            mountPath: "/var/lib/mysql"
          ...
      volumes:
        - name: mysql-data
          persistentVolumeClaim:
            claimName: mysql-pvc
        ...
```

### Attribution d'une adresse Ip externe au service

Si un *load balancer* est accessible à partir du cluster 
(le *load balancer* est généralement fournit par l'hébergeur du cluster kubernetes) 
et qu'il reste des adresses IP disponibles, 
il est possible d'attribuer une adresse IP externes aux services 
(en utilisant un [service de type `LoadBalancer`](https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer)).  
Un exemple est donné dans la dernière version de l'application 
pour le service du chat : [k8s/chat-svc.yaml](k8s/chat-svc.yaml).  
Sur le cluster utilisé pour le déploiement, cela attribue une adresse ip externe distincte à chaque service de type `LoadBalancer`.  
Par exemple si le service de *chat php* se voit attribuer l'adresse IP 
*37.59.31.132*, il sera accessible au moyen de l'url http://37.59.31.132.

### Utilisation d'un `Ingress` 

Dans le cas de services de type web, autre solution pour exposer un service vers l'extérieur 
est de l'exposer au moyen d'une ressource de type 
[`Ingress`](https://kubernetes.io/docs/concepts/services-networking/ingress/), ce qui suppos qu'un 
[`Ingress Controler`](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/) 
ait été préalablement déployé sur le cluster.

Si c'est le cas les ressources de type [`Ingress`](https://kubernetes.io/docs/concepts/services-networking/ingress/) 
permettent de mettre en place des règles de routages pour les requêtes entrantes de l'IP associés à l'`ingress controler`.  
Ces règles permettent de rediriger les requêtes entrantes, en fonction de nom de l'hôte utilisé dans les requêtes entrantes 
ou des préfixes des requêtes http, vers un service ou un autre.

Un exemple d'`ingress` est donné dans la dernière version de l'application 
pour le service du chat : [k8s/chat-ingress.yaml](k8s/chat-ingress.yaml). 
Il redirige toutes les requêtes envoyées sur l'hôte http://chat.demo.learninglab.eu 
vers le service correspondant au chat php.

### Gestion de la montée en charge de l'application

Dans la première version de l'application (branche [feature/v1](../../tree/feature/v1)), 
à cause de l'utilisation des sessions php, il n'était pas possible 
d'augmenter le nombre de pods de la partie web/php pour répondre 
à une augmentation de la charge de l'application. 

Dans cette seconde version, un serveur `Redis` est utilisé pour gérer les sessions php.  
Cela permet, puisque la partie web/php est maintenant sans état, 
de gérer les variations de la charge de travail au niveau de la partie web/php 
en augmentant ou en diminuant le nombre de pods.

Si l'addon metrics-server (minikube / microk8s) ou l'application 
[metrics-server](https://github.com/kubernetes-sigs/metrics-server) 
est activée sur le cluster, alors on pourra également utiliser la fonctionnalité de 
[mise à l'échelle horizontale](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/)
pour adapter le nombre de pods (de la partie php) à la charge travail. 

La mise à l'échelle horizontale peut être mise en place soit en utilisant une commande `kubectl` : 

```bash
kubectl autoscale --namespace chat deployment chat --max=10 --min=1 --cpu-percent=80
```

soit en utilisant une ressource k8s de type 
[`HorizontalPodAutoscaler`](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/).

Dans la configuration utilisée pour l'exemple, [k8s/chat-hpa.yaml](k8s/chat-hpa.yaml), 
le nombre de pods variera entre 1 et 10 en essayant de cibler, pour les pods de la partie php,
une utilisation moyenne de 80% de la demande (*requests.cpu*) CPU définie 
dans le déploiement de la partie web ([k8s/chat-dep.yaml](k8s/chat-dep.yaml)) :

```yaml
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: chat
  labels:
    app: chat
    tiers: backend
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: chat
  minReplicas: 1
  maxReplicas: 10
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 80
```

Pour voir fonctionner la mise à l'échelle, on peut charger artificiellement la partie web/php 
en utilisant par exemple l'utilitaire *apache bench* 
([https://httpd.apache.org/docs/2.4/fr/programs/ab.html](https://httpd.apache.org/docs/2.4/fr/programs/ab.html)). 

La commande suivante :  

```sh
kubectl run test --rm -i -n chatdemo --image=stalb/ab -- ab -c 80 -n 100000 http://chat/chat.php?pseudo=Joe\&chat=1
```

permettra de d'exécuter 100 000 requêtes (avec 80 requêtes en parallèles), 
ce qui devrait suffisamment charger le serveur pour provoquer, au bout de quelques minutes, 
une augmentation du nombre de pods de la partie web. 

Une fois toutes les requêtes effectuées, la diminution de la charge de travail 
devrait également entraîner à terme une diminution du nombre de pods 
(à noter que la descente est plus lente que la montée).

## Ajout des probes sur les conteneurs

Kubernetes permet d'ajouter des [*probes*](https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-probes) 
sur les conteneurs afin de vérifier 

- s'ils ont fini de démarrer ([`startupProbe`](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-startup-probes)), 
- s'ils sont toujours vivants ([`livenessProbe`](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-a-liveness-http-request)) ou 
- s'ils sont momentanément indisponibles ([`readinessProbe`](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-readiness-probes))

Par exemple, pour les conteneurs redis on ajoute les *probes* suivants 
dans le déploiement [k8s/redis-dep.yaml](k8s/redis-dep.yaml):

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis
  labels:
    app: chat
    tiers: session-cache
spec:
  ...
  template:
    metadata:
      labels:
        app: chat
        tiers: session-cache
    spec:
      containers:
      - name: redis
        image: docker.io/library/redis:7.2.1
        livenessProbe:
          exec:
            command:
              - "redis-cli"
              - "ping"
        startupProbe:
          failureThreshold: 10
          periodSeconds: 5
          tcpSocket:
            port: 6379
        ...
      ...
      ...
```

Le `startupProbe` considèrera que le conteneur a fini de démarrer quand le port `6379` commence à être utilisable (1 test toutes les 5 secondes et un temps limite de démarrage de 50 secondes).

Le `livenessProbe` lui utilisera la commande `redis-cli ping` 
pour tester si serveur redis est vivant. 

Dans le cas des conteneurs `Apache/PHP`, on fera des requêtes http pour
vérifier s'ils répondent toujours :

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: chat
    tiers: backend
  name: chat
spec:
  ...
  template:
    metadata:
      labels:
        app: chat
        lang: php
    spec:
      containers:
      - image: registry.gitlab.com/docker-exemple/chat_k8s/chat:2.1.0
        name: chat
        livenessProbe:
          httpGet:
            path: '/'
            port: 80
        ...
      ...
```

## Attente du démarrage des services `mysql` et `redis`

Dans le cas du déploiement de la partie `Apache/PHP` ([k8s/chat-dep.yaml](k8s/chat-dep.yaml)), 
comme on souhaite que les conteneurs attendent, avant de démarrer, 
que le service `mysql` et le service `redis` soient disponibles, 
on ajoute également deux [`initContainers`](https://kubernetes.io/docs/concepts/workloads/pods/init-containers/) pour attendre le démarrage des deux services : 
les conteneurs de la partie `containers` ne démarrent que lorsque les conteneurs de ma partie `initContainers` ont terminé (avec succès) leur exécution.

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: chat
    tiers: backend
  name: chat
spec:
  ...
  template:
    metadata:
      labels:
        app: chat
        lang: php
    spec:
      initContainers:
        - name: wait-for-redis
          image: docker.io/library/busybox:1.36.1
          command:
            - "sh"
            - "-c"
            - "until nc redis 6379 ; do sleep 5; done"
        - name: wait-for-mysql
          image: docker.io/library/busybox:1.36.1
          command:
            - "sh"
            - "-c"
            - "until nc mysql 3306 ; do sleep 5; done"
      containers:
      - image: registry.gitlab.com/docker-exemple/chat_k8s/chat:2.1.0
        name: chat
        ...
      ...
```

## Quelques liens utiles

- Documentation Minikube : https://minikube.sigs.k8s.io/docs/
- Documentation Microk8s : https://microk8s.io/docs
- Documentation kubernetes : https://kubernetes.io/docs/home/
- Tutoriels kubernetes : 
    - Minikube : https://kubernetes.io/docs/tutorials/hello-minikube/
    - bases de kubernetes (tutoriel interactif) : https://kubernetes.io/docs/tutorials/kubernetes-basics/
- Référence : 
    - kubectl : https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands
    - kubernetes API : https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/
- Mise à l'échelle horizontale avec kubernetes : https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/
- Configurer les Liveness, Readiness et Startup Probes : https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-a-tcp-liveness-probe
- Kubernetes Metrics Server : https://github.com/kubernetes-sigs/metrics-server
- Exemple de mise en oeuvre d'une base mysql répliquée dans kubernetes : https://kubernetes.io/docs/tasks/run-application/run-replicated-stateful-application/
- Exemple de mise en oeuvre d'une base Redis répliquée dans kubernetes : https://schoolofdevops.github.io/ultimate-kubernetes-bootcamp/13_redis_statefulset/
