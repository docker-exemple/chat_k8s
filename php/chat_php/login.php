<?php
    session_start();
    if (isset($_REQUEST["pseudo"])) $pseudo = $_REQUEST["pseudo"];
    elseif (isset ($_SESSION["pseudo"])) $pseudo = $_SESSION["pseudo"];
    else $pseudo = '';
    if (isset($_REQUEST["chat"])) $chat = $_REQUEST["chat"];
    elseif (isset($_SESSION["chat"])) $chat = $_SESSION["chat"];
    if (empty ($chat)) $chat = 1;
    session_write_close();
?>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <form method="POST" action="chat.php">
            <p>Connexion à un chat : </p>
            <p>
               <span>Pseudo  : </span>
               <input type="text" name="pseudo" value="<?= $pseudo ?>" /><br>
               <span>Chat id : </span>
               <input type="text" name="chat" value="<?= $chat ?>" /><br>
               <input type="submit" name="login" value="Connexion" />
            </p>
        </form>
        <p>Retour à la <a href="index.html">page d'accueil</a></p>
    </body>
</html>